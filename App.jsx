import React from 'react';
import AppContainer from './src/routes';
import Toast from 'react-native-toast-message';

function App() {
  return (
    <>
      <AppContainer />
      <Toast ref={(ref) => Toast.setRef(ref)} />
    </>
  );
}
export default App;
