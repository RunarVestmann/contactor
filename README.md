**Contacts app written in React Native (Expo)**

Contacts screen

![Contacts screen](screenshots/0contacts_screen.png)

Adding a contact

![Contact creation](screenshots/1contact_creation.png)

Viewing contact details

![Contact details](screenshots/2contact_details.png)

Edit contact details

![Edit contact details](screenshots/3contact_details_edited.png)

Select and delete contacts

![Contact selection](screenshots/4contacts_selected.png)
