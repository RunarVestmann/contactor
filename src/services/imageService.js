import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';
import fileService from './fileService';

const getPermission = async (permissionTypes) => {
  await Promise.all(
    // eslint-disable-next-line no-return-await
    permissionTypes.map(async (type) => await Permissions.askAsync(type)),
  );
};

async function addImage(image) {
  const data = await fileService.loadImage(image);
  return `data:image/jpeg;base64,${data}`;
}

async function takePhoto() {
  await getPermission([Permissions.CAMERA, Permissions.CAMERA_ROLL]);
  const result = await ImagePicker.launchCameraAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    quality: 0.8,
    base64: true,
    aspect: [16, 9],
  });

  const photo = result.cancelled ? '' : result.base64;
  if (photo.length > 0) return await `data:image/jpeg;base64,${photo}`;
  return undefined;
}

async function selectImageFromCameraRoll() {
  await getPermission([Permissions.CAMERA_ROLL]);
  const result = await ImagePicker.launchImageLibraryAsync({
    mediaTypes: ImagePicker.MediaTypeOptions.Images,
    quality: 0.8,
    base64: true,
    aspect: [16, 9],
  });

  const photo = result.cancelled ? '' : result.base64;
  if (photo.length > 0) return `data:image/jpeg;base64,${photo}`;
  return undefined;
}

export default {
  addImage, takePhoto, selectImageFromCameraRoll,
};
