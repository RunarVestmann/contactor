import Toast from 'react-native-toast-message';

const position = 'top';
const visibilityTime = 2500;
const topOffset = 250;

function displayToastSuccess(text1, text2) {
  Toast.show({
    type: 'success',
    position: position,
    text1,
    text2,
    visibilityTime: visibilityTime,
    topOffset: topOffset,
  });
}

function displayToastError(text1, text2) {
  Toast.show({
    type: 'error',
    position: position,
    text1,
    text2,
    visibilityTime: visibilityTime,
    topOffset: topOffset,
  });
}

export default {
  displayToastSuccess,
  displayToastError,
};
