import { Linking } from 'react-native';

export const call = async (telnr) => {
  Linking.openURL(`tel:${telnr}`);
};
