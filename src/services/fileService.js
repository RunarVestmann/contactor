// eslint-disable-next-line import/no-extraneous-dependencies
import * as FileSystem from 'expo-file-system';

const fileDirectory = `${FileSystem.documentDirectory}data`;

// eslint-disable-next-line consistent-return
const onException = (cb, errorHandler) => {
  try {
    return cb();
  } catch (err) {
    if (errorHandler) {
      return errorHandler(err);
    }
  }
};

async function cleanDirectory() {
  await FileSystem.deleteAsync(fileDirectory);
}

// eslint-disable-next-line no-return-await
async function copyFile(file, newLocation) {
  await onException(() =>
    FileSystem.copyAsync({
      from: file,
      to: newLocation,
    }),
  );
}

// eslint-disable-next-line no-return-await
async function loadImage(fileName) {
  await onException(() =>
    FileSystem.readAsStringAsync(`${fileName}`, {
      encoding: FileSystem.EncodingType.Base64,
    }),
  );
}

async function loadFile(fileName) {
  await onException(() => FileSystem.readAsStringAsync(`${fileName}`, {}));
}

async function addFile(fileName) {
  await FileSystem.makeDirectoryAsync(fileDirectory, { intermediates: true });
  // eslint-disable-next-line consistent-return
  await copyFile(fileName, `${fileDirectory}/${fileName}`);

  return await loadFile(fileName);
}

// eslint-disable-next-line no-return-await
async function removeImage(name) {
  await onException(() =>
    FileSystem.deleteAsync(`${fileDirectory}/${name}`, { idempotent: true }),
  );
}

async function setupDirectory() {
  const dir = await FileSystem.getInfoAsync(fileDirectory);
  if (!dir.exists) {
    await FileSystem.makeDirectoryAsync(fileDirectory);
  }
}

async function getAllImages() {
  // Check if directory exists
  await setupDirectory();

  const result = await onException(() =>
    FileSystem.readDirectoryAsync(fileDirectory),
  );
  return Promise.all(
    result.map(async (fileName) => ({
      name: fileName,
      type: 'image',
      file: await loadImage(fileName),
    })),
  );
}

export default {
  addFile,
  getAllImages,
  removeImage,
  loadImage,
  copyFile,
  cleanDirectory,
};
