import * as Contacts from 'expo-contacts';
import * as FileSystem from 'expo-file-system';
import latinize from 'latinize';
import toasterService from './toasterService';

const fileDirectory = `${FileSystem.documentDirectory}data`;

async function setupDataDirectory() {
  const dir = await FileSystem.getInfoAsync(fileDirectory);
  if (!dir.exists) {
    await FileSystem.makeDirectoryAsync(fileDirectory);
  }
}

async function getContactsFromPhone() {
  try {
    const { status } = await Contacts.requestPermissionsAsync();
    if (status === 'granted') {
      const { data } = await Contacts.getContactsAsync({
        fields: [Contacts.PHONE_NUMBERS, Contacts.EMAILS],
      });
      return data;
    }
    return [];
  } catch (err) {
    toasterService.displayToastError(
      'Error retrieving contacts',
      'Could not retrieve contacts from your phone',
    );
    return [];
  }
}

async function getStoredContactFilenames() {
  const filenamePromises = await FileSystem.readDirectoryAsync(fileDirectory);
  return await Promise.all(filenamePromises);
}

async function getStoredContacts() {
  try {
    const filenames = await getStoredContactFilenames();

    // Get all json strings in the files
    const jsonStringPromises = filenames.map((filename) =>
      FileSystem.readAsStringAsync(`${fileDirectory}/${filename}`),
    );
    const jsonStrings = await Promise.all(jsonStringPromises);

    // Parse all the json strings into objects
    const contacts = jsonStrings.map((jsonString) => JSON.parse(jsonString));
    return contacts || [];
  } catch (error) {
    toasterService.displayToastError(
      'Error retrieving contacts',
      'Could not retrieve your contacts',
    );
    return [];
  }
}

async function removeAllContacts() {
  try {
    const filenames = await getStoredContactFilenames();
    const removalPromises = filenames.map((filename) =>
      FileSystem.deleteAsync(`${fileDirectory}/${filename}`),
    );
    await Promise.all(removalPromises);
  } catch (error) {}
}

async function getStoredContactFilenamesContainingId(id) {
  const allFilenames = await getStoredContactFilenames();
  return allFilenames.filter((filename) => {
    const filenameSplit = filename.split('-');
    filenameSplit.splice(0, 1);
    return filenameSplit.join('-').replace('.json', '') === id;
  });
}

async function removeContact(id) {
  try {
    const filenamesContainingContactId = await getStoredContactFilenamesContainingId(
      id,
    );
    const promises = [];
    filenamesContainingContactId.forEach((filename) => {
      promises.push(FileSystem.deleteAsync(`${fileDirectory}/${filename}`));
    });
    await Promise.all(promises);
  } catch (error) {
    toasterService.displayToastError(
      'Error removing contacts',
      'Could not remove contacts',
    );
  }
}

async function removeContacts(idArray) {
  const filenamePromises = [];
  idArray.forEach((id) => {
    filenamePromises.push(getStoredContactFilenamesContainingId(id));
  });

  const filenameList = await Promise.all(filenamePromises);

  const deletePromises = [];
  filenameList.forEach((list) => {
    list.forEach((filename) => {
      deletePromises.push(
        FileSystem.deleteAsync(`${fileDirectory}/${filename}`),
      );
    });
  });
  await Promise.all(deletePromises);
}

async function editContact(id, name, phoneNumber, image) {
  try {
    const obj = {
      id,
      name,
      phoneNumber: phoneNumber.replace(/\s+/g, ''),
      image,
    };
    // name = name.replace(/\W/g, '');
    name = latinize(name);

    const nameWithoutSpace = name.replace(/\s+/g, '_').toLowerCase();
    const nameWithoutDashes = nameWithoutSpace.replace('-', '');
    const contactFilename = `${nameWithoutDashes}-${id}.json`;
    await setupDataDirectory();

    const filenamesContainingContactId = await getStoredContactFilenamesContainingId(
      id,
    );
    const promises = [];
    filenamesContainingContactId.forEach((filename) => {
      if (contactFilename !== filename) {
        promises.push(FileSystem.deleteAsync(`${fileDirectory}/${filename}`));
      }
    });
    promises.push(
      FileSystem.writeAsStringAsync(
        `${fileDirectory}/${contactFilename}`,
        JSON.stringify(obj),
      ),
    );

    await Promise.all(promises);
  } catch (error) {
    toasterService.displayToastError(
      'Error editing contact',
      'Could not edit contact',
    );
  }
}

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
    const r = (Math.random() * 16) | 0;
    const v = c === 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

async function saveContact(name, phoneNumber, image) {
  try {
    const guid = uuidv4();
    const obj = {
      id: guid,
      name,
      phoneNumber: phoneNumber.replace(/\s+/g, ''),
      image,
    };

    name = latinize(name);
    const nameWithoutSpace = name.replace(/\s+/g, '_').toLowerCase();
    const nameWithoutDashes = nameWithoutSpace.replace('-', '');
    const filename = `${nameWithoutDashes}-${guid}.json`;
    await setupDataDirectory();
    await FileSystem.writeAsStringAsync(
      `${fileDirectory}/${filename}`,
      JSON.stringify(obj),
    );
  } catch (error) {
    toasterService.displayToastError(
      'Error creating contact',
      'Could not create contact',
    );
  }
}

const alphabet = 'aábcdðeéfghiíjklmnoópqrstuúvwxyzþæö';
function alphabeticalOrder(a, b) {
  const name1 = a.name.toLowerCase();
  const name2 = b.name.toLowerCase();
  // Find the first position were the strings do not match
  let position = 0;
  while (name1[position] === name2[position]) {
    // If both are the same don't swap
    if (!name1[position] && !name2[position]) return 0;
    // Otherwise the shorter one goes first
    if (!name1[position]) return 1;
    if (!name2[position]) return -1;
    position++;
  }
  // Then sort by the characters position
  return alphabet.indexOf(name1[position]) - alphabet.indexOf(name2[position]);
}

export default {
  saveContact,
  editContact,
  getContactsFromPhone,
  getStoredContacts,
  removeAllContacts,
  removeContact,
  alphabeticalOrder,
  removeContacts,
};
