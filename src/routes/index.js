import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import Contacts from '../views/Contacts';
import ContactDetails from '../views/ContactDetails';
import Importer from '../views/Importer';

export default createAppContainer(
  createStackNavigator({
    Contacts: {
      screen: Contacts,
      navigationOptions: {
        headerStyle: {
          height: 75,
        },
        headerTitleStyle: {
          fontSize: 22,
          fontWeight: '900',
        },
        title: 'My Contacts',
        headerTitleAlign: 'center',
      },
    },
    ContactDetails: {
      screen: ContactDetails,
      navigationOptions: {
        headerStyle: {
          height: 75,
        },
        headerTitleStyle: {
          fontSize: 22,
          fontWeight: '900',
        },
        title: 'Contact Details',
        headerTitleAlign: 'center',
      },
    },
    Importer: {
      screen: Importer,
      navigationOptions: {
        headerStyle: {
          height: 75,
        },
        headerTitleStyle: {
          fontSize: 22,
          fontWeight: '900',
        },
        title: 'Import Your Contacts',
        headerTitleAlign: 'center',
      },
    },
  }),
);
