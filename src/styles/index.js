export const basicButton = {
  width: 100,
  height: 44,
  paddingTop: 10,
  paddingBottom: 10,
  paddingLeft: 20,
  paddingRight: 20,
  borderColor: 'rgba(40,40,40, .9)',
  borderWidth: 2,
  backgroundColor: 'rgba(252, 137, 22, 0.7)',
  borderRadius: 20,
};
export const basicButtonText = {
  fontWeight: 'bold',
  color: 'rgb(81, 81, 81)',
  textAlign: 'center',
};

export const basicInputText = {
  alignSelf: 'center',
  borderWidth: 0.5,
  borderColor: '#20232a',
  height: 30,
  width: '80%',
  marginTop: 20,
  paddingLeft: 10,
};
export const basicBox = {
  width: '95%',
  borderWidth: 1,
  borderColor: 'rgba(130, 130, 130, 0.5)',
  backgroundColor: 'rgba(242, 242, 242, 0.5)',
  paddingTop: 3,
  paddingLeft: 1,
  paddingBottom: 3,
  marginLeft: 7,
  marginRight: 7,
  borderRadius: 10,
};
