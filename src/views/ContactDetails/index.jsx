import React from 'react';
import { ImageBackground } from 'react-native';
import ContactDetail from '../../components/ContactDetail';
import bgImage from '../../../assets/bg2.jpg';

class ContactDetails extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      contactInfo: props.navigation.getParam('contactInfo', {}),
    };
  }

  render() {
    const {
      contactInfo: { id, name, image, phoneNumber },
      handleOnSubmit,
    } = this.state;
    return (
      <ImageBackground style={{ flex: 1 }} source={bgImage}>
        <ContactDetail
          id={id}
          name={name}
          image={image}
          phoneNumber={phoneNumber}
          onSubmit={handleOnSubmit}
        />
      </ImageBackground>
    );
  }
}

export default ContactDetails;
