import { StyleSheet } from 'react-native';
import { basicButton, basicButtonText } from '../../styles';
import { globalFontColor } from '../../styles/colors';

export default StyleSheet.create({
  button: {
    ...basicButton,
    justifyContent: 'center',
    width: 155,
  },
  buttonText: {
    ...basicButtonText,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 20,
    marginBottom: 20,
  },
  importText: {
    fontSize: 20,
    marginVertical: 10,
    textAlign: 'center',
    color: globalFontColor,
  },
});
