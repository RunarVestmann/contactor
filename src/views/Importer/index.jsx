import React from 'react';
import { View, Text, TouchableOpacity, ImageBackground } from 'react-native';
import contactService from '../../services/contactService';
import toastService from '../../services/toasterService';
import ImportList from '../../components/ImportList';
import Spinner from '../../components/Spinner';
import styles from './styles';
import bgImage from '../../../assets/bg2.jpg';

class Importer extends React.Component {
  constructor() {
    super();
    this.state = {
      contacts: [],
      refresh: true,
      selectAll: false,
      loadingImports: true,
    };
  }

  async componentDidMount() {
    this.setState({ loadingImports: true });
    const response = await contactService.getContactsFromPhone();
    this.setState({ loadingImports: false });
    const newArray = [];
    response.forEach((element) => {
      if (element.name) {
        let contactPhone = '';

        try {
          contactPhone = element.phoneNumbers[0].number;
        } catch (err) {}

        const contact = {
          id: element.id,
          name: element.name,
          phone: contactPhone,
          import: false,
        };
        newArray.push(contact);
      }
    });
    this.setState({ contacts: newArray });
  }

  onPress(item) {
    const { contacts, refresh } = this.state;
    const contact = contacts.find((t) => t.id === item);
    contact.import = !contact.import;
    this.setState({ refresh: !refresh });
  }

  selectAll() {
    const { contacts, refresh, selectAll } = this.state;
    contacts.forEach((elem) => {
      elem.import = !selectAll;
    });
    this.setState({ refresh: !refresh, selectAll: !selectAll });
  }

  async importSelected() {
    const { contacts } = this.state;
    const contactsTobeImported = contacts.filter((t) => t.import);
    await Promise.all(
      contactsTobeImported.map((contact) =>
        contactService.editContact(
          contact.id,
          contact.name,
          contact.phone,
          undefined,
        ),
      ),
    );
    if (contactsTobeImported.length > 0) {
      toastService.displayToastSuccess(
        'Contacts Imported',
        'Selected contacts successfully imported',
      );
    } else {
      toastService.displayToastError(
        'No contacts imported',
        'no contacts selected for importing',
      );
    }
  }

  render() {
    const { contacts, refresh, selectAll, loadingImports } = this.state;
    contacts.sort(contactService.alphabeticalOrder);
    return loadingImports ? (
      <Spinner />
    ) : (
      <ImageBackground style={{ flex: 1 }} source={bgImage}>
        <View style={styles.buttonContainer}>
          <TouchableOpacity
            onPress={() => this.selectAll()}
            style={styles.button}
          >
            {selectAll ? (
              <Text style={styles.buttonText}>Unselect All</Text>
            ) : (
              <Text style={styles.buttonText}>Select All</Text>
            )}
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => this.importSelected()}
            style={styles.button}
          >
            <Text style={styles.buttonText}>Import Selected</Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.importText}>
          Total number of contacts: {contacts.length}
        </Text>
        <ImportList
          contacts={contacts}
          refresh={refresh}
          onPress={(item) => this.onPress(item)}
        />
      </ImageBackground>
    );
  }
}
export default Importer;
