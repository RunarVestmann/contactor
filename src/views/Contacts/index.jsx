import React from 'react';
import { View, ImageBackground } from 'react-native';
import ContactThumbnailList from '../../components/ContactThumbnailList';
import Toolbar from '../../components/Toolbar';
import SearchBar from '../../components/SearchBar';
import bgImage from '../../../assets/bg2.jpg';

import contactService from '../../services/contactService';

class Contacts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      searchTerm: '',
      contacts: [],
      selectedContacts: [],
    };
    const { navigation } = props;
    navigation.addListener('didFocus', async (payload) => {
      const cont = await contactService.getStoredContacts();
      cont.sort(contactService.alphabeticalOrder);
      this.setState({ contacts: cont });
    });

    props.navigation.addListener('didBlur', () => {
      this.setState({ selectedContacts: [] });
    });
  }

  handleContactLongPress(pressedContactId) {
    const { selectedContacts } = this.state;
    if (selectedContacts.indexOf(pressedContactId) === -1) {
      this.setState({
        selectedContacts: [...selectedContacts, pressedContactId],
      });
    } else {
      this.setState({
        selectedContacts: selectedContacts.filter(
          (id) => id !== pressedContactId,
        ),
      });
    }
  }

  render() {
    const { searchTerm, contacts, selectedContacts } = this.state;

    return (
      <ImageBackground style={{ flex: 1 }} source={bgImage}>
        <View style={{ height: '100%' }}>
          <SearchBar
            placeholder="Search contact names"
            onChangeText={(text) =>
              this.setState({ searchTerm: text.toLowerCase() })
            }
          />
          <Toolbar
            size={24}
            isSomethingSelected={selectedContacts.length > 0}
            selectionCount={selectedContacts.length}
            onDeletePressed={async () => {
              await contactService.removeContacts(selectedContacts);
              const updatedContacts = await contactService.getStoredContacts();
              updatedContacts.sort(contactService.alphabeticalOrder);
              this.setState({
                selectedContacts: [],
                contacts: updatedContacts,
              });
            }}
          />

          <ContactThumbnailList
            contacts={contacts.filter(
              (contact) =>
                contact.name.toLowerCase().indexOf(searchTerm) !== -1,
            )}
            searchTerm={searchTerm}
            selectedContacts={selectedContacts}
            onContactLongPress={(pressedContactId) =>
              this.handleContactLongPress(pressedContactId)
            }
          />
        </View>
      </ImageBackground>
    );
  }
}

export default Contacts;
