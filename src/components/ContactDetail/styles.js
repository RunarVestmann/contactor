import { StyleSheet } from 'react-native';
import {
  basicButton,
  basicButtonText,
  basicInputText,
  basicBox,
} from '../../styles';
import { globalFontColor } from '../../styles/colors';

export default StyleSheet.create({
  button: {
    ...basicButton,
  },
  buttonText: {
    ...basicButtonText,
  },
  input: {
    ...basicInputText,
    fontSize: 22,
  },
  image: {
    width: 200,
    height: 200,
    borderRadius: 30,
    borderWidth: 1,
    margin: 10,
  },
  container: {
    flexDirection: 'column',
    width: '100%',
    borderWidth: 0,
    marginTop: 30,
    alignItems: 'center',
  },
  editCancel: {
    position: 'absolute',
    right: 3,
  },
  nameField: {
    fontSize: 22,
    color: globalFontColor,
  },
  telField: {
    fontSize: 18,
    color: globalFontColor,
  },
  subContainer: {
    ...basicBox,
    width: 300,
  },
  box: {
    width: '100%',
    alignItems: 'center',
  },
});
