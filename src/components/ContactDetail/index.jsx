import React from 'react';
import {
  View,
  Text,
  TextInput,
  Image,
  KeyboardAvoidingView,
  ScrollView,
  Button,
} from 'react-native';
import PropTypes from 'prop-types';
import { TouchableHighlight } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
import styles from './styles';
import profilePic from '../../../assets/defaultProfilePic.png';
import ImageToolbar from '../ImageToolbar';
import Call from '../Call';
import contactService from '../../services/contactService';
import toastService from '../../services/toasterService';

class ContactDetail extends React.Component {
  constructor(props) {
    super(props);
    const {
      name, image, phoneNumber, id,
    } = props;
    this.state = {
      name,
      image,
      phoneNumber,
      inputChanged: false,
      edit: !id,
      isNew: !id,
    };
  }

  async handleSubmit() {
    const { navigation } = this.props;
    const { id } = this.props;
    const { name, phoneNumber, image } = this.state;
    if (!name) {
      toastService.displayToastError('Invalid name', 'Please enter name');
      return;
    }
    if (id) {
      if (!/^[0-9\-\+]{7,}$/.test(phoneNumber)) {
        toastService.displayToastError(
          'Invalid phone number',
          'Please enter a phone number that contains at least 7 digits',
        );
        return;
      }
      await contactService.editContact(id, name, phoneNumber, image);
      toastService.displayToastSuccess(
        'Contact updated',
        'The contact information has been updated successfully',
      );
    } else {
      if (!/^[0-9\-\+]{7,}$/.test(phoneNumber)) {
        toastService.displayToastError(
          'Invalid input',
          'Please enter a phone number that contains at least 7 digits',
        );
        return;
      }
      await contactService.saveContact(name, phoneNumber, image);
      toastService.displayToastSuccess(
        'Contact created',
        'The contact has been created successfully',
      );
    }
    navigation.goBack();
  }

  renderInput() {
    const { name, phoneNumber } = this.state;
    return (
      <View>
        <TextInput
          style={styles.input}
          defaultValue={name}
          placeholder="Enter name"
          onChangeText={(text) => {
            this.setState({ inputChanged: true, name: text });
          }}
        />
        <TextInput
          style={styles.input}
          defaultValue={phoneNumber}
          keyboardType="number-pad"
          placeholder="Enter phone number"
          onChangeText={(text) => {
            this.setState({ inputChanged: true, phoneNumber: text });
          }}
        />
      </View>
    );
  }

  renderText() {
    const { name, phoneNumber } = this.state;
    return (
      <View style={styles.box}>
        <Text style={styles.nameField}>{name}</Text>
        <Text style={styles.telField}>{phoneNumber}</Text>
      </View>
    );
  }

  cancel() {
    const { name, image, phoneNumber } = this.props;
    this.setState({
      name,
      image,
      phoneNumber,
    });
  }

  render() {
    const {
      isNew, image, phoneNumber, inputChanged, edit,
    } = this.state;
    return (
      <ScrollView>
        <View style={styles.container}>
          {!isNew && (
            <View style={styles.editCancel}>
              <Button
                title={edit ? 'Cancel' : 'Edit'}
                onPress={() => {
                  if (edit) {
                    this.cancel();
                  }
                  this.setState({ edit: !edit });
                }}
              />
            </View>
          )}
          <Image
            source={image !== undefined ? { uri: image } : profilePic}
            style={styles.image}
          />
          {edit && (
            <ImageToolbar
              size={40}
              onImageAdded={(newImage) => {
                this.setState({ image: newImage, inputChanged: true });
              }}
            />
          )}
          <KeyboardAvoidingView
            style={{ flexDirection: 'column', marginTop: 10 }}
            behavior="padding"
          >
            <View
              style={[styles.subContainer, { paddingBottom: edit ? 25 : 5 }]}
            >
              {edit ? this.renderInput() : this.renderText()}
            </View>

            {!edit && phoneNumber !== '' && (
              <Call size={50} phoneNr={phoneNumber} />
            )}
            {edit && (
              <View style={{ alignItems: 'center', marginTop: 10 }}>
                <TouchableHighlight
                  onPress={() => this.handleSubmit()}
                  disabled={!inputChanged}
                  activeOpacity={0.6}
                  underlayColor="rgba(145, 145, 145, 0.3)"
                  style={[
                    styles.button,
                    { opacity: inputChanged ? 1 : 0.5 },
                    { marginTop: phoneNumber !== undefined ? 5 : 30 },
                  ]}
                >
                  <Text style={styles.buttonText}>Submit</Text>
                </TouchableHighlight>
              </View>
            )}
          </KeyboardAvoidingView>
        </View>
      </ScrollView>
    );
  }
}

ContactDetail.propTypes = {
  id: PropTypes.string,
  name: PropTypes.string,
  image: PropTypes.string,
  phoneNumber: PropTypes.string,
};

ContactDetail.defaultProps = {
  id: '',
  name: '',
  image: undefined,
  phoneNumber: '',
};

export default withNavigation(ContactDetail);
