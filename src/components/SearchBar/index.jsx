import React from 'react';
import { TextInput } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

const SearchBar = ({ placeholder, onChangeText }) => (
  <TextInput
    placeholder={placeholder}
    style={styles.searchBar}
    onChangeText={(text) => onChangeText(text)}
  />
);

SearchBar.propTypes = {
  placeholder: PropTypes.string,
  onChangeText: PropTypes.func.isRequired,
};

SearchBar.defaultProps = {
  placeholder: 'Enter search term',
};
export default SearchBar;
