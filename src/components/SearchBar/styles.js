import { StyleSheet } from 'react-native';
import { basicInputText } from '../../styles';

export default StyleSheet.create({
  searchBar: {
    ...basicInputText,
  },
});
