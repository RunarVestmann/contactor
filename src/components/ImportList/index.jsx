import React from 'react';
import { FlatList, View } from 'react-native';
import PropTypes from 'prop-types';
import ImportContact from '../ImportContact';

const ContactThumbnailList = ({ contacts, onPress, refresh }) => (
  <FlatList
    data={contacts}
    renderItem={({ item }) => (
      <ImportContact contact={item} onPress={onPress} />
    )}
    numColumns={1}
    extraData={(contacts, refresh)}
    keyExtractor={(contact) => contact.id.toString()}
    ItemSeparatorComponent={() => (
      <View
        style={{
          height: 5,
          alignSelf: 'center',
          width: '90%',
        }}
      />
    )}
  />
);

ContactThumbnailList.propTypes = {
  contacts: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      phoneNumber: PropTypes.string,
      image: PropTypes.string,
    }),
  ),
  onPress: PropTypes.func.isRequired,
  refresh: PropTypes.bool,
};

ContactThumbnailList.defaultProps = {
  contacts: [],
  refresh: false,
};
export default ContactThumbnailList;
