import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Text, TouchableHighlight } from 'react-native';
import { withNavigation } from 'react-navigation';
import styles from './styles';

const Toolbar = ({
  navigation,
  isSomethingSelected,
  onDeletePressed,
  selectionCount,
}) => (
  <View style={styles.container}>
    <TouchableOpacity
      onPress={() => navigation.navigate('ContactDetails')}
      style={styles.button}
    >
      <Text style={styles.buttonText}>Add</Text>
    </TouchableOpacity>
    <TouchableOpacity
      onPress={() => navigation.navigate('Importer')}
      style={styles.button}
    >
      <Text style={styles.buttonText}>Import</Text>
    </TouchableOpacity>
    <TouchableHighlight
      onPress={onDeletePressed}
      style={[styles.button, { opacity: isSomethingSelected ? 1 : 0.4 }]}
      disabled={!isSomethingSelected}
      underlayColor="rgba(145, 145, 145, 0.3)"
    >
      <View style={{ flexDirection: 'row' }}>
        <Text style={[styles.buttonText]}>Delete</Text>
        <Text
          style={[
            styles.counterCircle,
            { opacity: isSomethingSelected ? 1 : 0 },
          ]}
        >
          {selectionCount}
        </Text>
      </View>
    </TouchableHighlight>
  </View>
);

Toolbar.propTypes = {
  isSomethingSelected: PropTypes.bool,
  onDeletePressed: PropTypes.func.isRequired,
  selectionCount: PropTypes.number,
};

Toolbar.defaultProps = {
  isSomethingSelected: false,
  selectionCount: 0,
};

export default withNavigation(Toolbar);
