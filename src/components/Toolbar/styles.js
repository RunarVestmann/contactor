import { StyleSheet } from 'react-native';
import { basicButton, basicButtonText } from '../../styles';
import { globalIconColor } from '../../styles/colors';

export default StyleSheet.create({
  button: {
    ...basicButton,
  },
  buttonText: {
    ...basicButtonText,
    justifyContent: 'center',
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 20,
    marginBottom: 20,
  },
  toolbarItem: {
    paddingBottom: 20,
  },
  counterCircle: {
    justifyContent: 'center',
    fontSize: 12,
    height: '80%',
    textAlign: 'center',
    borderRadius: 12,
    borderWidth: 1,
    backgroundColor: 'rgba(70,70,70, .7)',
    color: 'white',
    marginLeft: 5,
    marginRight: -10,
    paddingLeft: 5,
    paddingRight: 5,
    paddingTop: 1,
    paddingBottom: 5,
  },
  icon: {
    color: globalIconColor,
  },
});
