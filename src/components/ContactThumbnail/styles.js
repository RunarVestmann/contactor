import { StyleSheet } from 'react-native';
import { basicBox } from '../../styles';
import { globalFontColor } from '../../styles/colors';

export default StyleSheet.create({
  image: {
    width: 30,
    height: 30,
    borderRadius: 30,
    borderWidth: 1,
    marginLeft: 5,
  },
  container: {
    ...basicBox,
    flexDirection: 'row',

  },
  name: {
    paddingTop: 7,
    paddingLeft: 10,
    fontSize: 16,
    color: globalFontColor,
  },
});
