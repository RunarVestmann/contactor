import React from 'react';
import { Text, Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import profilePic from '../../../assets/defaultProfilePic.png';
import styles from './styles';

const ContactThumbnail = ({
  id,
  name,
  image,
  phoneNumber,
  isSelected,
  navigation: { navigate },
  onLongPress,
}) => (
  <TouchableOpacity
    style={[
      styles.container,
      {
        backgroundColor: isSelected
          ? 'rgba(70,70,70, .7)'
          : 'rgba(255,255,255, .7)',
      },
    ]}
    onPress={() =>
      navigate('ContactDetails', {
        contactInfo: { id, name, image, phoneNumber },
      })
    }
    onLongPress={() => onLongPress(id)}
  >
    <Image
      source={image !== undefined ? { uri: image } : profilePic}
      style={styles.image}
    />
    <Text style={[styles.name, isSelected ? { color: 'white' } : {}]}>
      {name}
    </Text>
  </TouchableOpacity>
);

ContactThumbnail.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  image: PropTypes.string,
  phoneNumber: PropTypes.string,
  isSelected: PropTypes.bool,
  onLongPress: PropTypes.func,
};

ContactThumbnail.defaultProps = {
  image: undefined,
  phoneNumber: '',
  isSelected: false,
  onLongPress: () => {},
};

export default withNavigation(ContactThumbnail);
