import React from 'react';
import { FlatList, View } from 'react-native';
import PropTypes from 'prop-types';
import ContactThumbnail from '../ContactThumbnail';

const ContactThumbnailList = ({
  contacts,
  selectedContacts,
  onContactLongPress,
}) => (
  <FlatList
    data={contacts}
    extraData={(contacts, selectedContacts)}
    renderItem={({ item: { id, name, image, phoneNumber } }) => (
      <ContactThumbnail
        id={id}
        name={name}
        image={image}
        phoneNumber={phoneNumber}
        isSelected={selectedContacts.indexOf(id) !== -1}
        onLongPress={(pressedContactId) => onContactLongPress(pressedContactId)}
      />
    )}
    numColumns={1}
    keyExtractor={(contact) => contact.id.toString()}
    ItemSeparatorComponent={() => (
      <View
        style={{
          height: 10,
          alignSelf: 'center',
          width: '90%',
        }}
      />
    )}
  />
);

ContactThumbnailList.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  contacts: PropTypes.arrayOf(PropTypes.object),
  selectedContacts: PropTypes.arrayOf(PropTypes.string),
  onContactLongPress: PropTypes.func,
};

ContactThumbnailList.defaultProps = {
  contacts: [],
  selectedContacts: [],
  onContactLongPress: () => {},
};

export default ContactThumbnailList;
