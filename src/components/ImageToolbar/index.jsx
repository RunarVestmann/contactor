import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { SimpleLineIcons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import imageService from '../../services/imageService';
import styles from './styles';

async function handleCameraPress(onImageAdded) {
  const photo = await imageService.takePhoto();
  if (photo) onImageAdded(photo);
}

async function handleImagePress(onImageAdded) {
  const photo = await imageService.selectImageFromCameraRoll();
  if (photo) onImageAdded(photo);
}

const ImageToolbar = ({ size, onImageAdded }) => (
  <View style={styles.container}>
    <TouchableOpacity onPress={() => handleCameraPress(onImageAdded)}>
      <SimpleLineIcons style={styles.icon} name="camera" size={size} />
    </TouchableOpacity>
    <TouchableOpacity onPress={() => handleImagePress(onImageAdded)}>
      <SimpleLineIcons style={styles.icon} name="picture" size={size} />
    </TouchableOpacity>
  </View>
);

ImageToolbar.propTypes = {
  size: PropTypes.number,
  onImageAdded: PropTypes.func.isRequired,
};

ImageToolbar.defaultProps = {
  size: 40,
};

export default ImageToolbar;
