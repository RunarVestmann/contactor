import { StyleSheet } from 'react-native';

import { globalIconColor } from '../../styles/colors';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
  icon: {
    marginTop: 10,
    marginHorizontal: 20,
    color: globalIconColor,
  },
});
