import React from 'react';
import { View, TouchableOpacity } from 'react-native';
import { SimpleLineIcons } from '@expo/vector-icons';
import PropTypes from 'prop-types';
import * as phoneService from '../../services/phoneService';

import styles from './styles';

function handlePhonePress(phoneNr) {
  phoneService.call(phoneNr);
}
const Call = ({ phoneNr, size }) => (
  <View style={styles.container}>
    {phoneNr && (
      <TouchableOpacity onPress={() => handlePhonePress(phoneNr)}>
        <SimpleLineIcons style={styles.icon} name="phone" size={size} />
      </TouchableOpacity>
    )}
  </View>
);

Call.propTypes = {
  phoneNr: PropTypes.string,
  size: PropTypes.number,
};

Call.defaultProps = {
  phoneNr: '',
  size: 50,
};

export default Call;
