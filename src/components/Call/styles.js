import { StyleSheet } from 'react-native';
import { globalIconColor } from '../../styles/colors';

export default StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  icon: {
    marginTop: 15,
    margin: 10,
    marginHorizontal: 20,
    color: globalIconColor,

  },
});
