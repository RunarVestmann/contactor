import { StyleSheet } from 'react-native';

import { globalFontColor } from '../../styles/colors';

export default StyleSheet.create({
  name: {
    paddingTop: 5,
    paddingLeft: 20,
    fontSize: 18,
    color: globalFontColor,
  },
  phone: {
    textAlign: 'right',
    flexGrow: 1,
    paddingTop: 5,
    paddingRight: 15,
    fontSize: 18,
    color: globalFontColor,
  },
  checkMark: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    left: 1,
    fontSize: 16,
    flexDirection: 'row',
    color: globalFontColor,
  },
});
