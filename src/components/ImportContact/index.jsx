import React from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Fontisto } from '@expo/vector-icons';

import styles from './styles.js';

const ImportContact = ({ contact, onPress }) => (
  <TouchableOpacity onPress={() => onPress(contact.id)}>
    <View
      style={{ flexDirection: 'row', paddingLeft: 10, alignContent: 'center' }}
    >
      <View style={styles.checkMark}>
        <Fontisto
          name={contact.import ? 'checkbox-active' : 'checkbox-passive'}
          size={20}
        />
      </View>
      <Text style={styles.name}>{contact.name}</Text>
      <Text style={styles.phone}>{contact.phone}</Text>
    </View>
  </TouchableOpacity>
);

ImportContact.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  contact: PropTypes.object.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default ImportContact;
